= 连接方式概览

Transwarp ArgoDB 支持通过 Beeline、应用程序、外部工具等多种方式连接，本小节介绍在不同认证模式下，如何连接 ArgoDB 数据库。

[cols="30,~"]
|===
|连接方式 |说明

|xref:connect-via-beeline.adoc[通过 Beeline 命令行连接]
|通过命令行方式执行 SQL 命令，如创建数据库。

|通过 Waterdrop 客户端连接
|通过客户端界面化方式管理数据库，支持跨平台管理多种数据库（如 Apache Hive）。


|通过 JDBC 连接
.2+|支持标准的 JDBC/ODBC 接口，安装驱动后即可编写应用程序，读写 ArgoDB 的数据。
|通过 ODBC 连接
|===
[TIP]
====
除此以外，ArgoDB 还支持使用标准 JDBC/ODBC 接口的工具进行连接，例如 Tableau 等。
====